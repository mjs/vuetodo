import Proxy from './Proxy';

export default class TaskProxy extends Proxy {

  constructor(parameters = {}) {
    super('api/tasks', parameters);
  }
}
