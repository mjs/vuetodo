
export const tasks = state => state.tasks;

export const taskList = state => state.taskList;

export const hasNoTask = state => state.taskList.length === 0;

export const getTaskById = (state, getters) => id => getters.tasks[id];

export const getValidTasks = (state, getters) =>
  getters.taskList.map(id => getters.getTaskById(id));

export default {
  tasks,
  taskList,
  hasNoTask,
  getTaskById,
  getValidTasks,
};
