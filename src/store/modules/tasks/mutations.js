import _union from 'lodash/union';
import {
  ADD_TASK,
  REMOVE_TASK,
  MARK_TASK_AS_COMPLETED,
  MARK_TASK_AS_INCOMPLETED,
} from './mutation-types';

export default {
  [ADD_TASK](state, task) {
    state.tasks = { ...state.tasks, [task.id]: task };
    state.taskList = _union([task.id], state.taskList);
  },

  [REMOVE_TASK](state, taskId) {
    state.taskList = state.taskList.filter(x => x !== taskId);
  },

  [MARK_TASK_AS_COMPLETED](state, { id }) {
    state.tasks[id].is_completed = true;
  },

  [MARK_TASK_AS_INCOMPLETED](state, { id }) {
    state.tasks[id].is_completed = false;
  },
};
