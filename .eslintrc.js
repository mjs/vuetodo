// http://eslint.org/docs/user-guide/configuring
module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module',
  },
  env: {
    browser: true,
  },
  extends: 'airbnb-base',
  plugins: [
    'html',
    'import',
    'promise',
  ],
  'settings': {
    'import/resolver': {
      'webpack': {
        'config': 'build/webpack.base.conf.js',
      },
    },
  },
  'rules': {
    'no-param-reassign': 0,
    'no-multi-assign': 0,
    'import/extensions': ['error', 'always', {
      'js': 'never',
      'vue': 'never',
    }],
    'import/no-extraneous-dependencies': ['error', {
      'optionalDependencies': ['test/unit/index.js']
    }],
    'comma-dangle': ['error', {
      'arrays': 'always-multiline',
      'objects': 'always-multiline',
      'imports': 'always-multiline',
      'exports': 'always-multiline',
      'functions': 'ignore',
    }],
    'no-trailing-spaces': ['error', {'skipBlankLines': true}],
    'quotes': [2, 'single'],
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
  },
}
