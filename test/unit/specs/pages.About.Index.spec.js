import { shallow, createLocalVue } from 'vue-test-utils';
import PagesAboutIndex from '@/pages/About/Index';

import Layout from './__mocks__/Layout';

const localVue = createLocalVue();

describe('PagesAboutIndex', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(PagesAboutIndex, { localVue, stubs: { 'core-layout': Layout } });
  });

  it('should render a message', () => {
    expect(wrapper.vm.$el.textContent).to.include('A simple todo app');
  });
});
