import Vue from 'vue';
import TaskEmpty from '@/components/TaskEmpty';

describe('TaskEmpty', () => {
  let vm;

  beforeEach(() => {
    vm = new Vue(TaskEmpty).$mount();
  });

  it('should render a message', () => {
    expect(vm.$el.textContent).to.include('Your task list looks empty');
  });
});
