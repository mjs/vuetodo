/* eslint-disable import/prefer-default-export */

export const vmNextTick = vm => new Promise((resolve) => {
  vm.$nextTick(() => {
    resolve();
  });
});
