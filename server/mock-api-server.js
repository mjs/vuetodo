const express = require('express');
const sqlite3 = require('sqlite3');
const bodyParser = require('body-parser');

const jsonParser = bodyParser.json();
const db = new sqlite3.Database('./todo.db');
const app = express();

function dbErrorHandler(err) {
  this.status(500).send();
  throw err;
}

// enable CORS
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.get('/api/tasks', function (req, res) {
  const stmt = db.prepare('SELECT * FROM tasks ORDER BY id DESC');
  stmt.all([], (err, rows) => {
    err && dbErrorHandler.call(res, err);
    res.json(rows);
  });
});

app.post('/api/tasks', jsonParser, function (req, res) {
  const { title } = req.body;
  const stmt = db.prepare('INSERT INTO tasks (title, is_completed) VALUES ((?), (?))');
  stmt.run([title, 0], function (err) {
    err && dbErrorHandler.call(res, err);
    db.get('SELECT * FROM tasks WHERE id=?', [this.lastID], (err, row) => {
      err && dbErrorHandler.call(res, err);
      res.json(row);
    });
  });
});

app.put('/api/tasks/:taskId', jsonParser, function (req, res) {
  const { taskId } = req.params;
  const { title, is_completed } = req.body;
  const stmt = db.prepare('UPDATE tasks SET title=?, is_completed=? WHERE id=?');
  stmt.run([title, is_completed, taskId], function (err) {
    err && dbErrorHandler.call(res, err);
    db.get('SELECT * FROM tasks WHERE id=?', [this.lastID], (err, row) => {
      err && dbErrorHandler.call(res, err);
      res.json(row);
    });
  });
});

app.delete('/api/tasks/:taskId', function (req, res) {
  const { taskId } = req.params;
  const stmt = db.prepare('DELETE FROM tasks WHERE id=?');
  stmt.run([taskId], (err) => {
    err && dbErrorHandler.call(res, err);
    res.json({ status: 'ok' });
  });
});

// eslint-disable-next-line no-unused-vars
const server = app.listen(3000);
